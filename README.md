# gatsby-starter-default
[Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Install

```
npm install --global gatsby-cli
```

And run from your CLI:
```sh
gatsby new gatsby-example-site
```

Then you can run it by:
```sh
cd gatsby-example-site
gatsby develop
```
